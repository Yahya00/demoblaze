package TestCase;

import Pages.DashBoard;
import Pages.HomePage;
import Pages.LoginPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class TestLogin {
    public static void main(String[] args) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver.exe");
        WebDriver driver = new ChromeDriver();

        driver.manage().window().maximize();
        driver.get("https://www.demoblaze.com/");

        HomePage home = new HomePage(driver);
        LoginPage login = new LoginPage(driver);
        DashBoard dashboard = new DashBoard(driver);

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        home.clickLoginBtn();

        login.enterUsername("asudahlah@mailinator.com");
        login.enterPassword("Yahya00");
        login.clickLoginBtn();

        dashboard.veryLogin();

        driver.quit();

    }
}

package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class RegisterPage {

    WebDriver driver;

    public RegisterPage (WebDriver driver) {
        this.driver = driver;
    }

    By signUserName = By.id("sign-username");
    By signPassword = By.id("sign-password");
    By registerBtn = By.className("btn btn-primary");

    public void EnterName(String user) {
        driver.findElement(signUserName).sendKeys(user);
    }

    public void EnterSignPassword(String pass) {
        driver.findElement(signPassword).sendKeys(pass);
    }

    public void ClickregusterBtn() {
        driver.findElement(registerBtn).click();
    }
}

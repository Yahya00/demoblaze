package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DashBoard {
    WebDriver driver;
    WebDriverWait wait = new  WebDriverWait(driver,5);

    public DashBoard (WebDriver driver) {
        this.driver = driver;

    }

    By searchField = By.id("");
    By logoutBtn = By.id("");
    By verifyLogin = By.id("nameofuser");


    public void enterSearchField(String searchValue) {
        driver.findElement(searchField).sendKeys(searchValue);
    }

    public void clickLogoutBtn() {
        driver.findElement(logoutBtn).click();
    }
    public void veryLogin() {
        driver.findElement(verifyLogin).getLocation();


    }
}

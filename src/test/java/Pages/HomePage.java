package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


public class HomePage {
    WebDriver driver;

    public HomePage(WebDriver driver) {
        this.driver = driver;
    }

        By loginBtn = By.linkText("Log in");
        public void clickLoginBtn () {
            driver.findElement(loginBtn).click();
        }

        By signUpBtn = By.id("signin2");
        public void clickSignupBtn () {
            driver.findElement(signUpBtn).click();
        }

        //By navbar = By.id("navbarExample");
        //public String getNavbar () {
          //  String nav = driver.findElement(navbar).getText();
          //  return nav;}

    }


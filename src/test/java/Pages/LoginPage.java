package Pages;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage {
    WebDriver driver;

    public LoginPage (WebDriver driver) {

        this.driver = driver;
    }

    By userName = By.id("loginusername");
    By password = By.id("loginpassword");
    By loginBtn = By.className("btn btn-primary");


    public void enterUsername(String user) {
        driver.findElement(userName).sendKeys(user);
    }

    public void enterPassword(String pass) {
        driver.findElement(password).sendKeys(pass);
    }

    public void clickLoginBtn() {
        driver.findElement(loginBtn).click();
    }
}
